# poky

Forked Yocto poky repository for voxl. This will not produce an image that you
can run on voxl, but it is useful to produce packages that will run on voxl. For
example, in the modalai-sd820-jethro branch, the configuration  in modalai/local.conf was changed to
build the avahi package and all of it's dependencies. Once the build completes
you can try the functionality in Qemu. When everything is working the desired
packages can be copied from the poky/build/tmp/deploy/ipk/aarch64 directory
onto the target board and installed.

*  $ git clone -b modalai-sd820-jethro https://gitlab.com/voxl-public/poky.git
*  $ cd poky
*  $ Modify modalai/local.conf to add the desired packages
*  $ source oe-init-build-env
*  $ cp ../modalai/*.conf conf/
*  $ bitbake core-image-full-cmdline
*  $ runqemu qemuarm64 nographic

Commit that shows how Avahi was integrated is [here](https://gitlab.com/voxl-public/poky/commit/430d17280c7df7e67f9dc5a7125c671d842150b0)